import os
os.environ["CUDA_VISIBLE_DEVICES"]=""

import torch
assert not torch.cuda.is_available()
import copy
import time

import gridtools.helpers as gh
import numpy as np
from matplotlib import pyplot as plt

import gridtools.helpers
import gridtools.paths

import linkgistic.data
import linkgistic.models


@gh.dumpArgs(abbreviation_nchars=1024)
def raw_main(
    seed = 0, # random seed
    pos = (0,), # positive class label(s)
    neg = None, # negative class label(s) (None means complement of pos)
    ntrain = None, # number of training points
    ntest = None, # number of testing points
    datatype = 'mnist', # fmnist, kmnist, mnist
    epochs = (2000, 600, 600, 600, 600, 600, 600, 600), # epochs for inner loop (M-step of EM) ; length = number of EM steps
    hyper_opt = False, # optimise hyper parameters
    weight_decay = 0.01, # regularisation of parameters in M step
    hotstart = True, # keep params from last M step
    period=100, # trig kernel period
    halfM=48, # trig kernel basis size
    a=1.2, # trig kernel a
    b=1.0, # trig kernel b ; parameterised as k(0,0), the prior variance of the underlying GP
    params0_dict=dict(mu=0.0, gamma=0.01, nu0=0.0, beta=1.0), # hyper parameters
    n_link_samples = 60, # link samples for stochastic EM (training)
    n_link_samples_test = 300, # link samples for testing
    #
    data_path=os.path.expanduser('~/data'), # directory containing subdirectories mnist, fmnist and kmnist
    basepath='/tmp/em_linkgistic_demo/', # where to save to disk
    subpath='default', # subdir to save to disk
    do_save=True, # whether to save to disk (logs and results)
    do_plot=True, # whether to plot link and source functions etc
    write_pdf=True, # whether to save plots to pdf files
):

    t0 = time.time()

    if do_save:
        path = gridtools.paths.output('em_linkgistic_demo/' + subpath + ('/' if len(subpath) else ''), basepath=basepath)
        gridtools.helpers.dumpArgsToDirectory(path)
        tee = gridtools.helpers.Tee('%s/output.txt' % path, compress=False)
    else:
        path = None
        assert not write_pdf

    params0_dict['a'] = a
    params0_dict['b'] = b

    linkgistic.data._path = data_path
    Xtrain, Ytrain, Ytrainmulti, Xtest, Ytest, Ytestmulti = linkgistic.data.digit_data(pos, neg, ntrain, ntest, datatype, seed)
    np.random.seed(seed)

    d = Xtrain.shape[1]

    inner_model = linkgistic.models.LogisticRegression(input_dim=d, output_dim=1)
    link_model_sampler = linkgistic.models.SampledLinks
    link_model = linkgistic.models.LinkModel(hotstart=hotstart, hyper_opt=hyper_opt, period=period, halfM=halfM, params0_dict=params0_dict)

    model = linkgistic.models.EMLinkgistic(link_model=link_model, link_model_sampler=link_model_sampler, inner_model=inner_model, n_link_samples=n_link_samples, n_link_samples_test=n_link_samples_test, weight_decay=weight_decay, epochs=epochs, do_plot=do_plot, write_pdf=write_pdf, path=path)
    model.set_data(Xtrain, Ytrain, Xtest, Ytest)
    infos = model.optimise()

    if do_plot:
        plt.show()

    elapsed_time = time.time() - t0
    print('elapsed_time', gh.pretty_duration(elapsed_time))

    print('intial model (logistic regression) correct ratio', infos[0]['info_test_full_logres']['proportion_correct'])
    print('final linkgistic post E step correct ratio', infos[-1]['info_test_post_link']['proportion_correct'])
    print('final linkgistic post M step correct ratio', infos[-1]['info_test_post_inner']['proportion_correct'])

    if do_save:
        tee.to_file()

    return tuple((dict(
        path=path,
        infos=copy.deepcopy(infos),
        elapsed_time=elapsed_time,
    ),))


main = gh.auto_parser_inspector(allow_missing=True)(raw_main)


if __name__ == '__main__':

    main()