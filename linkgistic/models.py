import copy

import autograd.numpy
import autograd.numpy as np
import gridtools.helpers as gh
import matplotlib.pyplot as plt
import numpy as plain_numpy
import torch
import torch.optim
from autograd import grad, hessian
from matplotlib.backends.backend_pdf import PdfPages
from scipy.optimize import minimize

from linkgistic import helpers
from linkgistic.covariances import TrigKernel, _memo_psi_d

_figsize = (7.5, 3)
LOGHALF = float(np.log(0.5))


# stable way of averaging logits
def nus_to_pred(z, y):
    assert z.shape == y.shape, (z.shape, y.shape)
    xent = torch.nn.functional.binary_cross_entropy_with_logits(z, y, reduction='none')
    log_p = torch.logsumexp(-xent, dim=1) - autograd.numpy.log(y.shape[1])
    pred_y = log_p > LOGHALF
    return log_p, pred_y


# superclass
class Model(object):

    def __init__(self, nll, parameterisation):

        self.nll = nll
        self.parameterisation = parameterisation
        self.nhypoparams = parameterisation.nhypoparams
        self.nhyperparams = parameterisation.nhyperparams
        self.nparams = self.nhypoparams + self.nhyperparams
        self.hyperparams_slice = slice(self.nhypoparams, self.nparams)
        self.hypoparams_slice = slice(self.nhypoparams)

    def nlpost(self, params, x, y):

        params_dict = self.parameterisation.lin2named(params)

        assert len(x.shape) == len(y.shape) == 1
        rval = self.parameterisation.nlprior(params_dict)
        step = 2000
        for i0 in range(0, len(x), step):
            ind = slice(i0, i0 + step)
            rval = rval + self.nll(params_dict, x[ind], y[ind])
        return rval

    def zero_hyper_params(self, params):

        assert len(params) == self.nparams
        params[self.hyperparams_slice] = 0
        return params

    def init_params(self):

        params = np.random.randn(self.nparams) * 1e-3
        params = self.parameterisation.named2lin(**self.parameterisation.lin2named(params))
        _params = self.parameterisation.named2lin(**self.parameterisation.lin2named(params))
        assert np.allclose(params, _params)
        return params


# helper to handle parameterisation
class IsotonicRegressionModelParameterisation(object):

    def __init__(self, M, c, hyper_reg=0):

        self.M = M
        self.c = c
        self.hyper_reg = hyper_reg
        self.nhyperparams = 5
        self.nhypoparams = M + 1
        self.hypernames_raw = [r'\beta', r'a', r'b', r'\mu', r'\gamma']
        self.hypernames_parameterised = [r'\log(\beta)', r'\log(a-1)', r'\log(b)', r'\mu', r'\log(\gamma)']
        self.hyper_keys = ('beta', 'a', 'b', 'mu', 'gamma')
        self.hyper_reg_means = {'beta': np.log(0.05), 'a': np.log(1.2-1), 'b': np.log(0.2), 'mu': 0, 'gamma': np.log(0.1)}

    def lin2named(self, params, label = None):

        w = params[:self.M]
        nu0 = params[self.M]
        beta = np.exp(params[-5])
        a = 1 + np.exp(params[-4])
        b = np.exp(params[-3])
        mu = params[-2]
        gamma = np.exp(params[-1])
        kernel = TrigKernel(a, b, self.M, self.c)

        return dict(w=w, nu0=nu0, beta=beta, a=a, b=b, mu=mu, gamma=gamma, M=self.M, c=self.c, kernel=kernel)

    def named2lin(self, label = None, **kw):
        if label is not None:
            print('named2lin', label, 'beta', kw['beta'], 'a', kw['a'], 'b', kw['b'], 'M', kw['M'], 'c', kw['c'])
        params = np.concatenate((
            kw['w'],
            [
            kw['nu0'],
            np.log(kw['beta']),
            np.log(kw['a']-1),
            np.log(kw['b']),
            kw['mu'],
            np.log(kw['gamma']),
            ]
        ))
        return params

    def nlprior(self, params):

        a, b, M, mu, gamma, w, nu0, kernel = [params[_] for _ in ['a', 'b', 'M', 'mu', 'gamma', 'w', 'nu0', 'kernel']]

        lambdas = kernel.lambdas
        assert len(w) == len(lambdas)

        nlpw = -(
            - 0.5 * np.sum((w ** 2) / lambdas)
            - 0.5 * np.sum(np.log(lambdas))
            - 0.5 * M * np.log(2 * np.pi)
        )
        nlpnu0 = -(
            - 0.5 * gamma * (nu0 - mu) ** 2
            + 0.5 * np.log(gamma)
            - 0.5 * np.log(2 * np.pi)
        )

        return nlpw + nlpnu0

    def prior_q(self, params_dict):

        kernel =  TrigKernel(params_dict['a'], params_dict['b'], self.M, self.c)

        Sigma = np.zeros((self.nhypoparams, self.nhypoparams))
        Sigma[:-1, :-1] = np.diag(kernel.lambdas)
        Sigma[-1, -1] = 1.0 / params_dict['gamma']

        q = dict()
        q['mu'] = q['params_mode'] = np.concatenate((np.zeros(self.M), [params_dict['nu0']]))
        q['Sigma'] = Sigma
        q['kernel'] = kernel
        q['params_dict'] = params_dict

        return q


# sigmoid log lik for classification
def log_lik_sigmoid(f, y):
    return -np.logaddexp(0, -f * (2 * y - 1))


# sigmoid negative log lik for classification given model
def nll_sigmoid(params, x, y):

    assert len(x.shape) == 1
    assert y.shape == x.shape

    M, beta, w, nu0, kernel = [params[_] for _ in ['M', 'beta', 'w', 'nu0', 'kernel']]

    N = len(x)
    assert N == len(y)
    psi = kernel.psi(x, True)

    f = np.dot(np.dot(psi, w), w) + nu0

    lik = log_lik_sigmoid(f, y)

    return -lik.sum() + 1e-3 * beta**2


# laplace inference (ml2 with automatic diff based ML2 using implicit function theorem is not included here)
class ADLaplace(object):

    _hessian_ridge = 1e-7
    _minimize_method = 'L-BFGS-B'
    _precision_options = dict()

    def __init__(self, model):

        self.model = model

        self._minimize_options = dict(maxiter=50000)
        self._minimize_options_hypers = dict(maxiter=30, disp=True, ftol=0)
        self._minimize_options_map_within_mml = dict(ftol=100 * np.finfo(float).eps)
        self._hyper_ngrid = 20
        self._minimize_options.update(ADLaplace._precision_options)

    def init_params(self, params0, params0_dict):

        if params0 is None:
            params0 = self.model.init_params()
        if params0_dict is not None:
            d = self.model.parameterisation.lin2named(params0)
            d.update(**params0_dict)
            params0 = self.model.parameterisation.named2lin(label='init_params', **d)

        return params0

    def hypo_sample(self, q, num_samples):

        return helpers.sample_mvn(q['mu'], q['Sigma'], num_samples)

    def predict_samples(self, x, q, samples):

        psi = q['kernel'].psi(x, False)
        return samples[-1, :].reshape(1, -1) + outerise(psi, samples[:-1, :])

    def predict_mean(self, x, q):

        psi = q['kernel'].psi(x, False)

        return q['mu'][-1] + np.dot(np.dot(psi, q['mu'][:-1]), q['mu'][:-1]) + (psi * q['Sigma'][:-1, :-1].reshape(1, self.model.parameterisation.M, self.model.parameterisation.M)).sum(2).sum(1)

    def predict_samples_f(self, x, q, samples):

        return np.dot(q['kernel'].phi(x), samples[:-1, :])

    def predict_mean_f(self, x, q):

        return np.dot(q['kernel'].phi(x), q['mu'][:-1])

    def predict_prior_mean(self, x, prior_q):

        return x * prior_q['kernel'].kdiag() + prior_q['mu'][-1]

    # maximum a posteriori
    def map(self, params0, params0_dict, x, y, do_nll=False, minimize_disp=False):

        x = x.astype(float)
        y = y.astype(float)

        params0 = self.init_params(params0, params0_dict)

        def nlpost(params):
            return self.model.nlpost(params=params, x=x, y=y)
        grad_nlpost = grad(nlpost)
        grad_nlpost_no_hypers = lambda params: self.model.zero_hyper_params(grad_nlpost(params))
        hessian_nlpost = hessian(nlpost)

        options =self._minimize_options.copy()
        options.update(disp=minimize_disp)

        pb = gh.ProgressBar(n=options['maxiter'], freq=10, tfreq=10, label='MAP minimize for link posterior')
        callback = lambda _: pb()
        res = minimize(nlpost, params0, jac=grad_nlpost_no_hypers, method=ADLaplace._minimize_method, options=options, callback=callback)
        pb(label='(MAP final)', force_print=True)

        for k in ['message', 'status', 'success', 'fun']:
            print('MAP minimize', k, res[k])

        params_mode = res['x']
        hypoparams_mode = params_mode[self.model.hypoparams_slice]
        params_dict = self.model.parameterisation.lin2named(params_mode)
        kernel = params_dict['kernel']

        params_hessian = hessian_nlpost(params_mode)
        hypoparams_hessian = params_hessian[self.model.hypoparams_slice, self.model.hypoparams_slice]
        hypoparams_covariance = np.linalg.inv(hypoparams_hessian + ADLaplace._hessian_ridge * np.eye(self.model.nhypoparams))

        nll = dnll = None

        return dict(kernel=kernel, mu=hypoparams_mode, params_mode=params_mode, Sigma=hypoparams_covariance, nll=nll, dnll=dnll)

    @classmethod
    def make(cls, parameterisation):
        return ADLaplace(model=Model(nll=nll_sigmoid, parameterisation=parameterisation))

    @classmethod
    def plot_xt_classification(cls, x, t):
        ax = plt.axis()
        _, _, lo, hi = ax
        r = hi - lo
        y0 = lo + 0.45 * r
        y1 = lo + 0.55 * r
        plt.plot(x[t == 0], y0 + 0 * t[t == 0], 'r.', label=r'%i / %i negative training points' % (len(t[t == 0]), len(t)))
        plt.plot(x[t == 1], y1 + 0 * t[t == 1], 'b.', label=r'%i / %i positive training points' % (len(t[t == 1]), len(t)))

    def plot_nu(self, slabel, xtrain, ytrain, xgrid, q, num_samples, num_samples_gray, hypo_samples, hypo_samples_gray, ytrue_func_clean=None, true_func_latex=None, baselines=[]):

        g_mean_g_xgrid = self.predict_mean(xgrid, q)
        g_samples_xgrid = self.predict_samples(xgrid, q, hypo_samples)
        g_samples_gray_xgrid = self.predict_samples(xgrid, q, hypo_samples_gray)

        miny = min(g_mean_g_xgrid.flatten()) - 0.1
        maxy = max(g_mean_g_xgrid.flatten()) + 0.1
        if ytrain is not None:
            miny = min(miny, min(ytrain))
            maxy = max(maxy, max(ytrain))
        minx = min(xgrid)
        maxx = max(xgrid)

        plt.figure('nu %s' % slabel, figsize=_figsize)
        lines = plt.plot(xgrid, g_samples_gray_xgrid, 'gray', linestyle='-', linewidth=2, alpha=20 / num_samples_gray)
        lines[0].set_label(r'%i posterior samples of $\nu(x)$ (transparent)' % num_samples_gray)
        lines = plt.plot(xgrid, g_samples_xgrid, 'k', linestyle='-', linewidth=2, alpha=1)
        lines[0].set_label(r'%i posterior samples of $\nu(x)$' % num_samples)
        lines = plt.plot(xgrid, g_mean_g_xgrid.flatten(), 'r', linestyle='-', linewidth=2)
        lines[0].set_label(r'$\mathbb E[\nu(x)]$')
        for baseline_func, baseline_label, baseline_colour in baselines:
            lines = plt.plot(xgrid, baseline_func(xgrid), baseline_colour, linestyle='-', linewidth=2)
            lines[0].set_label(baseline_label)
        if ytrue_func_clean is not None:
            lines = plt.plot(xgrid, ytrue_func_clean(xgrid), 'lime', linestyle='-', linewidth=2)
            lines[0].set_label(r'true noiseless $%s$' % true_func_latex)
        plt.xlabel(r'input $x$')
        plt.axis([min(xgrid), max(xgrid), miny, maxy])
        if ytrain is not None:
            if set(ytrain).difference(set([0,1])) == set():
                ADLaplace.plot_xt_classification(xtrain, ytrain)
            else:
                plt.plot(xtrain, ytrain, 'b.', label=r'%i input data points' % (len(ytrain)))
        plt.legend(loc='upper left')
        plt.grid()

    def plot_p(self, slabel, xtrain, ttrain, xgrid, q, num_samples, num_samples_gray, hypo_samples, hypo_samples_gray, ytrue_func_clean=None, true_func_latex=None, squash_func=None):

        sigmoid_only = hypo_samples is None and hypo_samples_gray is None

        miny = 0
        maxy = 1
        minx = min(xgrid)
        maxx = max(xgrid)

        if not sigmoid_only:
            g_mean_g_xgrid = self.predict_mean(xgrid, q)
            g_samples_xgrid = self.predict_samples(xgrid, q, hypo_samples)
            g_samples_gray_xgrid = self.predict_samples(xgrid, q, hypo_samples_gray)

        plt.figure('t %s' % slabel, figsize=_figsize)
        if not sigmoid_only:
            lines = plt.plot(xgrid, squash_func(g_samples_gray_xgrid), 'gray', linestyle='-', linewidth=2, alpha=20 / num_samples_gray)
            lines[0].set_label(r'%i posterior samples of $\sigma \circ \nu(x)$ (transparent)' % num_samples_gray)
            lines = plt.plot(xgrid, squash_func(g_samples_xgrid), 'k', linestyle='-', linewidth=2, alpha=1)
            lines[0].set_label(r'%i posterior samples of $\sigma \circ \nu(x)$' % num_samples)
            lines = plt.plot(xgrid, squash_func(g_samples_gray_xgrid).mean(1), 'r', linestyle='-', linewidth=2)
            lines[0].set_label(r'$\mathbb E[\sigma \circ \nu(x)]$')
        if ytrue_func_clean is not None:
            lines = plt.plot(xgrid, squash_func(ytrue_func_clean(xgrid)), 'lime', linestyle='-', linewidth=2)
            lines[0].set_label(r'$\sigma \circ (\text{true noiseless } %s)$' % true_func_latex)
        if sigmoid_only:
            lines = plt.plot(xgrid, squash_func(xgrid), 'lime', linestyle='-', linewidth=2)
            lines[0].set_label(r'$\sigma$')

        xtrain_sma, ttrain_sma = helpers.sma(xtrain, ttrain, 64)
        plt.plot(xtrain_sma, ttrain_sma, 'y-', alpha=0.6, label=r'moving average of targets')
        plt.xlabel(r'input $x$')
        plt.axis([min(xgrid), max(xgrid), miny, maxy])
        ADLaplace.plot_xt_classification(xtrain, ttrain)
        plt.legend(loc='upper left')
        plt.grid()

    def plot_f(self, slabel, xtrain, ytrain, xgrid, q, num_samples, num_samples_gray, hypo_samples, hypo_samples_gray, ytrue_func_clean=None, ytrue_func_clean_derivative=None, true_func_latex=None, savefig=None):

        for absfn in (False, True):

            abs_maybe = (lambda x: x * x) if absfn else (lambda x: x)
            exp2 = '^2' if absfn else ''

            g_mean_g_xgrid_f = abs_maybe(self.predict_mean_f(xgrid, q))
            g_samples_xgrid_f = abs_maybe(self.predict_samples_f(xgrid, q, hypo_samples))
            g_samples_gray_xgrid_f = abs_maybe(self.predict_samples_f(xgrid, q, hypo_samples_gray))

            if ytrue_func_clean_derivative is not None:
                maxy = max(ytrue_func_clean_derivative(xgrid))
            else:
                maxy = np.max(np.mean(g_samples_gray_xgrid_f**2, axis=1)**0.5) * 1.5
            maxy = 1.05 * (maxy if absfn else (maxy ** 0.5))
            minx = min(xgrid)
            maxx = max(xgrid)
            miny = 0 if absfn else -maxy

            g_samples_xgrid_f.shape

            plt.figure('f abs %s %s' % (str(absfn), slabel), figsize=_figsize)
            lines = plt.plot(xgrid, g_samples_gray_xgrid_f, 'gray', linestyle='-', linewidth=2, alpha=20 / num_samples_gray)
            lines[0].set_label(r'%i posterior samples of $f%s(x)$ (transparent)' % (num_samples_gray, exp2))
            lines = plt.plot(xgrid, g_samples_xgrid_f, 'k', linestyle='-', linewidth=2, alpha=1)
            lines[0].set_label(r'%i posterior samples of $f%s(x)$' % (num_samples, exp2))
            if not absfn:
                lines = plt.plot(xgrid, g_mean_g_xgrid_f.flatten(), 'r', linestyle='-', linewidth=2)
                lines[0].set_label(r'$\mathbb E[f%s(x)]$' % exp2)
            if ytrue_func_clean_derivative is not None:
                if absfn:
                    lines = plt.plot(xgrid, ytrue_func_clean_derivative(xgrid), 'lime', linestyle='-', linewidth=2)
                    lines[0].set_label(r'derivative of true noiseless function')
                else:
                    lines = plt.plot(xgrid, ytrue_func_clean_derivative(xgrid) ** 0.5, 'lime', linestyle='-', linewidth=2)
                    lines = plt.plot(xgrid, -ytrue_func_clean_derivative(xgrid) ** 0.5, 'lime', linestyle='-', linewidth=2)
                    lines[0].set_label(r'$\pm \sqrt{\text{derivative of true noiseless function}}$')
            plt.xlabel(r'input $x$')
            plt.axis([min(xgrid), max(xgrid), miny, maxy])
            plt.legend(loc='upper left')
            plt.grid()
            if savefig is not None:
                savefig()


class LogisticRegression(torch.nn.Module):

    def __init__(self, input_dim, output_dim):
        super(LogisticRegression, self).__init__()
        self.linear = torch.nn.Linear(input_dim, output_dim)

    def forward(self, x):
        outputs = self.linear(x)
        return outputs


# this is a computational bottleneck:
def outerise(psi, w):
    return (np.tensordot(psi, w, axes=(2, 0)) * w.reshape(1, *w.shape)).sum(1)


# sampled links with some wrapper to let pytorch chain differentiate them
class SampledLinks(torch.autograd.Function):

    @staticmethod
    def forward(ctx, x, q, hypo_samples):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        ctx.save_for_backward(x)
        ctx.q = q
        ctx.hypo_samples = hypo_samples
        x_np = x.cpu().detach().numpy().flatten()

        nu = np.empty((len(x_np), hypo_samples.shape[1]), dtype=x_np.dtype)
        step = 2000
        for i0 in range(0, len(x_np), step):
            ind = slice(i0, i0+step)
            psi = q['kernel'].psi(x_np[ind], memoed=False)
            nu[ind, :] = hypo_samples[-1, :].reshape(1, -1) + outerise(psi, hypo_samples[:-1, :])
        return torch.as_tensor(nu).to(device)

    @staticmethod
    def backward(ctx, grad_output):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        x = ctx.saved_tensors[0]
        q = ctx.q
        hypo_samples = ctx.hypo_samples
        x_np = x.cpu().detach().numpy().flatten()
        phi = q['kernel'].phi(x_np)
        f = np.dot(phi, hypo_samples[:-1, :])
        f2 = f * f
        rval = torch.as_tensor(f2).to(device) * grad_output
        return rval, None, None


def printstats(x, s):
    print(s, 'min', np.min(x), 'max', np.max(x), 'mean', np.mean(x), 'std', np.std(x), 'nans', np.isnan(x).sum(), '/', len(x))


def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = plain_numpy.linalg.cholesky(B)
        return True
    except plain_numpy.linalg.LinAlgError:
        return False


def nearestPD(A, ridge):

    # N.J. Higham, "Computing a nearest symmetric positive semidefinite matrix"

    np = plain_numpy
    la = plain_numpy.linalg

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))
    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    maxeig = np.max(np.real(la.eigvals(A3)))
    A3 += I * np.abs(maxeig) * ridge

    return A3


class LinkModel(object):

    def __init__(self, hyper_opt=True, period=10, halfM=48, cov_type='trig', normed=True, params0_dict=dict(a=1.3, b=0.3, mu=0.0, gamma=0.1, nu0=0.0), hotstart=True, ensure_positive_definiteness=True):

        self.hyper_opt = hyper_opt
        self.period = period
        self.cov_type = cov_type
        self.halfM = halfM
        self.c = 1 / period
        self.ensure_positive_definiteness = ensure_positive_definiteness

        if normed:
            b = params0_dict['b']
            params0_dict['b'] = 1
            kdiag = IsotonicRegressionModelParameterisation(2*self.halfM, self.c).prior_q(params0_dict)['kernel'].kdiag()
            params0_dict['b'] = b / kdiag
            kdiag2 = IsotonicRegressionModelParameterisation(2 * self.halfM, self.c).prior_q(params0_dict)['kernel'].kdiag()
            assert np.allclose(kdiag2, b)
            print('normed b', kdiag2)

        self.params0_dict = params0_dict
        self.hotstart = hotstart
        self.parameterisation = IsotonicRegressionModelParameterisation(2*self.halfM, self.c)
        self.nll = nll_sigmoid
        self.model = ADLaplace.make(self.parameterisation)
        self.prior_q = self.model.model.parameterisation.prior_q(params0_dict)
        print('prior kdiag', self.prior_q['kernel'].kdiag())
        self.q = None
        self.params0 = None

    def fit(self, x, y):

        x = copy.deepcopy(x).astype(float)
        y = copy.deepcopy(y).astype(float)

        self.q = self.model.map(params0=self.params0, params0_dict=self.params0_dict, x=x, y=y)

        if self.ensure_positive_definiteness and not isPD(self.q['Sigma']):
            print('Sigma is not p.d. with eigenvalues:')
            print(sorted(plain_numpy.real(plain_numpy.linalg.eigvals(self.q['Sigma']))))
            self.q['Sigma'] = nearestPD(self.q['Sigma'], 1e-6)
            print('corrected eigenvalues:')
            print(sorted(plain_numpy.real(plain_numpy.linalg.eigvals(self.q['Sigma']))))

        if self.hotstart:
            self.params0 = self.q['params_mode']

        return self


class EMLinkgistic(object):

    def __init__(self, link_model, link_model_sampler, inner_model, n_link_samples, n_link_samples_test, epochs, weight_decay, num_samples_plot_gray=256, num_samples_plot=3, do_plot=False, write_pdf=False, path=None, save_debug=False):

        self.link_model = link_model
        self.link_model_sampler = link_model_sampler
        self.inner_model = inner_model
        self.n_link_samples = n_link_samples
        self.n_link_samples_test = n_link_samples_test
        self.epochs = epochs
        self.weight_decay = weight_decay
        self.num_samples_plot = num_samples_plot
        self.num_samples_plot_gray = num_samples_plot_gray
        self.do_plot = do_plot
        self.write_pdf = write_pdf
        self.path = path
        self.save_debug = save_debug
        self.gpu_maybe = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.inner_model.to(self.gpu_maybe)

        self.optimizer = torch.optim.LBFGS(self.inner_model.parameters())

    def set_data(self, X, Y, Xtest=None, Ytest=None, wtrue=None):

        self.X = X.astype(float)
        self.Y = Y.astype(float)
        self.nx, self.dx = X.shape
        assert Y.shape == (self.nx, 1), (X.shape, Y.shape)
        self.Xtest = Xtest.astype(float)
        self.Ytest = Ytest.astype(float)
        if Xtest is not None:
            self.nxtest, self.dxtest = Xtest.shape
            assert Ytest.shape == (self.nxtest, 1), (Xtest.shape, Ytest.shape)
            assert self.dxtest == self.dx
        self.wtrue = wtrue

        print('EMLinkgistic.set_data', self.X.shape, self.Xtest.shape)

    def plot_data(self, wold):

        plt.figure('data', figsize=_figsize)
        plt.clf()
        ipos = self.Y.flatten() == 1
        ineg = self.Y.flatten() == 0
        plt.plot(self.X[ipos, 0], self.X[ipos, 1], 'b.')
        plt.plot(self.X[ineg, 0], self.X[ineg, 1], 'r.')
        if self.wtrue is not None:
            plt.plot([0, self.wtrue[0]], [0, self.wtrue[1]], 'k', linewidth=12, label='wtrue', alpha=0.5)
            plt.plot([0, 0], [0, self.wtrue[2]], 'gray', linewidth=6, label='biastrue', alpha=0.5)
        if wold is not None:
            plt.plot([0, wold[0]], [0, wold[1]], 'g', linewidth=8, label='w', alpha=0.5)
            plt.plot([0, 0], [0, wold[2]], 'magenta', linewidth=3, label='bias', alpha=0.5)
        plt.legend(loc='upper left')

    def em_step(self, em_iter, epochs):

        self.inner_model.train()

        inner_maxiter = epochs
        ew_alpha = 0.

        if self.link_model is None or em_iter == 0:
            hypo_samples = None
        else:
            hypo_samples = self.link_model.model.hypo_sample(self.link_model.q, self.n_link_samples)

        pb = gh.ProgressBar(n=inner_maxiter, freq=1, tfreq=10)
        stats = dict()

        for inner_iter in range(inner_maxiter):

            def closure(stats=stats):

                self.optimizer.zero_grad()

                X = torch.tensor(self.X, dtype=torch.float).to(self.gpu_maybe)
                Y = torch.tensor(self.Y, dtype=torch.float).to(self.gpu_maybe)
                if not (self.link_model is None or em_iter == 0):
                    Y = Y.repeat(1, self.n_link_samples)

                f = self.inner_model(X)

                if hypo_samples is None:
                    nu = f
                else:
                    nu = self.link_model_sampler.apply(f, self.link_model.q, hypo_samples).to(self.gpu_maybe)

                logp, correcty = nus_to_pred(nu, Y)
                loss = -logp.mean()
                proportion_correct = np.mean(correcty.cpu().detach().numpy())

                regulariser = torch.sum(torch.zeros(1, dtype=torch.float)).to(self.gpu_maybe)
                for p in self.inner_model.parameters():
                    regulariser = regulariser + torch.sum(p**2)
                regularised_loss = loss + self.weight_decay * regulariser

                stats['loss'] = (1-ew_alpha) * float(loss.cpu().detach().numpy()) + ew_alpha * stats.get('loss', 0)
                stats['regulariser'] = (1-ew_alpha) * float(regulariser.cpu().detach().numpy()) + ew_alpha * stats.get('regulariser', 0)
                stats['regularised_loss'] = (1-ew_alpha) * float(regularised_loss.cpu().detach().numpy()) + ew_alpha * stats.get('regularised_loss', 0)
                stats['proportion_correct'] = (1-ew_alpha) * float(proportion_correct) + ew_alpha * stats.get('proportion_correct', 0)

                regularised_loss.backward()

                return regularised_loss

            self.optimizer.step(closure)

            force_print = inner_iter == (inner_maxiter - 1)
            pb(force_print=force_print, label='M of EM reg_loss {regularised_loss} loss {loss} reg {regulariser} pct correct {pct_correct:2.2f}'.format(loss=gh.pretty_number(float(stats['loss'])), regulariser=gh.pretty_number(float(stats['regulariser'])), regularised_loss=gh.pretty_number(float(stats['regularised_loss'])), pct_correct=100 * stats['proportion_correct']))

    def predict(self, X, Y, sample_link=True, store_predictions=False):

        self.inner_model.eval()

        Xin = X.copy()
        Yin = Y.copy()

        loss = 0
        proportion_correct = 0
        logp_pred = np.zeros(X.shape[0], dtype=float)
        f = np.zeros(X.shape[0], dtype=float)

        step = X.shape[0]
        for i0 in range(0, X.shape[0], step):
            ind = slice(i0, i0 + step)

            X = torch.tensor(Xin[ind, :], dtype=torch.float).to(self.gpu_maybe)
            Y = torch.tensor(Yin[ind, :], dtype=torch.float).to(self.gpu_maybe)

            if self.link_model is None or not sample_link:
                hypo_samples = None
            else:
                hypo_samples = self.link_model.model.hypo_sample(self.link_model.q, self.n_link_samples_test)
                Y = Y.repeat(1, self.n_link_samples_test)

            this_f = self.inner_model(X)
            f[ind] = this_f.cpu().detach().numpy().flatten()

            if hypo_samples is None:
                nu = this_f
            else:
                nu = SampledLinks.apply(this_f, self.link_model.q, hypo_samples)

            logp, correcty = nus_to_pred(nu, Y)
            loss = loss + logp.cpu().detach().numpy().sum()
            proportion_correct = proportion_correct + correcty.cpu().detach().numpy().astype(float).sum()

            this_logp_pred, _ = nus_to_pred(nu, torch.ones_like(nu))
            this_logp_pred = this_logp_pred.cpu().detach().numpy().flatten().astype(np.float)

            logp_pred[ind] = this_logp_pred

        loss = loss / X.shape[0]
        proportion_correct = proportion_correct / X.shape[0]

        if not store_predictions:
            logp_pred = None

        return dict(loss=loss, proportion_correct=proportion_correct, logp_pred=logp_pred), f

    def plot_link(self, f, label, ngrid=128, epsgrid=0.1):

        if self.do_plot and self.write_pdf:
            plt.close('all')

        if self.link_model.q is not None:
            hypo_samples = self.link_model.model.hypo_sample(self.link_model.q, self.num_samples_plot)
            hypo_samples_gray = self.link_model.model.hypo_sample(self.link_model.q, self.num_samples_plot_gray)
        else:
            hypo_samples = hypo_samples_gray = None
        xgrid = np.linspace(f.min() - epsgrid, f.max() + epsgrid, ngrid)
        squash_func = lambda x: 1 / (1 + np.exp(-x))
        if self.link_model.q is not None:
            self.link_model.model.plot_nu(label, f.flatten(), self.Y.flatten(), xgrid, self.link_model.q, self.num_samples_plot, self.num_samples_plot_gray, hypo_samples, hypo_samples_gray)
            self.savefig()
        self.link_model.model.plot_p(label, f.flatten(), self.Y.flatten(), xgrid, self.link_model.q, self.num_samples_plot, self.num_samples_plot_gray, hypo_samples, hypo_samples_gray, squash_func=squash_func)
        self.savefig()

    def optimise(self):

        sampled_link = None
        infos = []

        for em_iter, epochs_iter in enumerate(self.epochs):

            first_em_iter = (em_iter == 0)
            last_em_iter = (em_iter == (len(self.epochs)-1))

            if self.do_plot and self.write_pdf:
                pdffn = '%s/em_iter_%.3i.pdf' % (self.path, em_iter)
                pdf = PdfPages(pdffn)
                def savefig(pdf=pdf):
                    pdf.savefig()
                def closepdf(pdf=pdf):
                    pdf.close()
                    plt.close('all')
                self.savefig = savefig
            else:
                self.savefig = lambda : plt.pause(0.01)
                closepdf = lambda : None

            info = dict()

            self.em_step(em_iter, epochs_iter)
            if first_em_iter:
                state_dict = copy.deepcopy(self.inner_model.state_dict())
                info['info_train_part_logres'], _ = self.predict(self.X, self.Y, sample_link=not first_em_iter, store_predictions=False)
                info['info_test_part_logres'], _ = self.predict(self.Xtest, self.Ytest, sample_link=not first_em_iter, store_predictions=False)
                for ie, e in enumerate(self.epochs[1:]):
                    print('full logres epochs', ie+1, e)
                    self.em_step(em_iter, e)
                info['info_train_full_logres'], f = self.predict(self.X, self.Y, sample_link=not first_em_iter, store_predictions=False)
                info['info_test_full_logres'], _ = self.predict(self.Xtest, self.Ytest, sample_link=not first_em_iter, store_predictions=False)
                if self.do_plot:
                    self.inner_model.eval()
                    self.plot_link(f, label='n %i em_iter %i full logres' % (self.X.shape[0], em_iter))
                self.inner_model.load_state_dict(state_dict)

            self.inner_model.eval()
            info['train'], f = self.predict(self.X, self.Y, sample_link=not first_em_iter, store_predictions=False)

            if self.do_plot:
                self.plot_link(f, label = 'n %i em_iter %i post_inner' % (self.X.shape[0], em_iter))

            info['info_test_post_inner'], _ = self.predict(self.Xtest, self.Ytest, sample_link = not first_em_iter, store_predictions=False)

            if self.do_plot and self.dx == 2:
                self.plot_data(wold=None)

            if self.link_model is not None:

                _memo_psi_d.clear()
                printstats(f, 'f')
                if self.save_debug:
                    wh.dump(dict(f=f,Y=self.Y.flatten()), filename='%s/debug_%.3i.pkl' % (self.path, em_iter))
                self.link_model = copy.deepcopy(self.link_model).fit(f, self.Y.flatten().astype(float))

                if self.do_plot:
                    self.plot_link(f, label = 'n %i em_iter %i post_link' % (self.X.shape[0], em_iter))

                info['info_test_post_link'], _ = self.predict(self.Xtest, self.Ytest, sample_link=True, store_predictions=last_em_iter and False)

            infos.append(info)

            keys = sorted(set(sum([list(info.keys()) for info in infos], [])))
            for k in keys:
                for i, info in enumerate(infos):
                    if k in info:
                        print(i, k, info[k])

            closepdf()

        return infos
