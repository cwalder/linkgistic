import autograd.numpy as np

import linkgistic.helpers


# replace the diagonal of a rank three tensor in-place:
def replace_diag(trig, trig_diag):
    np.einsum('nii->ni', trig)[:, :] = trig_diag.reshape(trig_diag.shape[:2])
    return trig


# psi (integral of outer product of the basis functions phi, see the paper):
def psi(x, mvec, halfM, c):

    # unreadably optimised for speed

    x = x.reshape((-1, 1, 1))
    m = mvec.reshape((1, -1, 1))
    n = mvec.reshape((1, 1, -1))

    cpx = (c * np.pi) * x
    cnpx = n * cpx
    cmpx = m * cpx
    cn = np.cos(cnpx)
    sn = np.sin(cnpx)
    cm = cn.transpose(0, 2, 1)
    sm = sn.transpose(0, 2, 1)
    sm2 = np.sin(2 * cmpx)
    denom = (np.pi * c) * (m * m - n * n)
    np.einsum('ijj->ij', denom)[:] = 1

    idenom = 1 / denom

    tmps = n * cn * sm
    ss = (tmps - tmps.transpose(0,2,1)) * idenom
    tmpc = m * cn * sm
    cc = (tmpc-tmpc.transpose(0, 2, 1)) * idenom

    sc = (m - m * cm * cn - n * sm * sn) * idenom

    halfx = x * 0.5
    cmp = (c * np.pi) * m

    ssdiag = halfx - 0.25 * sm2 / cmp
    ccdiag = halfx + 0.25 * sm2 / cmp
    scdiag = 0.5 * sm * sm / cmp

    ss = replace_diag(ss, ssdiag)
    cc = replace_diag(cc, ccdiag)
    sc = replace_diag(sc, scdiag)

    rval = np.empty((len(x), 2*halfM, 2*halfM), dtype=float)
    rval[:, :halfM, :halfM] = ss
    rval[:, :halfM, halfM:] = sc
    rval[:, halfM:, :halfM] = sc.transpose((0, 2, 1))
    rval[:, halfM:, halfM:] = cc

    return rval


# to enable memoizing:
def _psi_flattened_args(x_mvec_halfM_c):

    x, mvec, halfM, c = x_mvec_halfM_c

    mvec = np.array(mvec)
    x = np.array(x)

    return psi(x, mvec, halfM, c)


# memoized psi
_memo_psi, _memo_psi_d = linkgistic.helpers.memodict_d(_psi_flattened_args)


# trigonometric kernel
class TrigKernel(object):

    def __init__(self, a, b, M, c, min_lambda=1e-7):

        # a, b, M, c are trigonometric kernel parameters
        # min_lambda is a numerical hack

        self.halfM = int(M/2)
        assert (2*self.halfM) == M
        self.M = M
        self.c = c
        self.a = a
        self.min_lambda = min_lambda
        self.mvec = np.arange(1, self.halfM+1)
        self.omegas = np.pi * self.mvec * self.c
        self._memo_psi_t = dict()
        self.set_b(b)

    def set_b(self, b):

        self.b = b
        self.half_spectrum = self.b / (self.a ** self.mvec)
        self.lambdas = np.concatenate((self.half_spectrum, self.half_spectrum))
        self.lambdas = np.maximum(self.lambdas, self.min_lambda)

    # k(0,0)
    def normaliser(self, a, M):

        return (1-a**(-M/2)) / (a-1)

    # the basis
    def phi(self, x):

        x_omegas = np.outer(x, self.omegas)
        return np.hstack((np.sin(x_omegas), np.cos(x_omegas)))

    # the derivative of the basis
    def dphi(self, x):

        x_omegas = np.outer(x, self.omegas)
        return np.hstack((self.omegas.reshape(1, -1) * np.cos(x_omegas), -self.omegas.reshape(1, -1) * np.sin(x_omegas)))

    # the kernel
    def k(self, x, z):

        # closed form available but not used here
        return np.dot(self.phi(x) * self.lambdas, self.phi(z).T)

    # the diagonal of the kernel
    def kdiag(self):

        return self.b * self.normaliser(self.a, self.M)

    # integral of the outer product of the basis
    def psi(self, x, memoed):
        if memoed:
            return _memo_psi((tuple(x), tuple(self.mvec), self.halfM, self.c))
        else:
            return psi(x, self.mvec, self.halfM, self.c)
