# Lingistic regression with the integrated squared Gaussian process

This repository accompanies the NeurIPS 2020 paper entitled "All your Loss are Belong to Bayes" by Christian Walder and Richard Nock:

https://arxiv.org/abs/2006.04633

## Installation (for python virtualenv)

### Get the code

Create a python3.8 virtualenv and activate it, then:

```
git clone https://gitlab.com/cwalder/linkgistic.git
cd linkgistic
```

This includes parts of `https://github.com/sorki/python-mnist` and `https://bitbucket.org/cwalder/gridtools/`. 

### Install dependencies

With pip:

```
pip install -r requirements.txt 
```

### Data

By default we assume the datasets are at the locations 
```
~/data/mnist/
~/data/fmnist/
~/data/kmnist/
```
but you can specify a different top level location than `~/data` using the `--data_path` option as demonstrated below.

The `mnist` data is available from `http://yann.lecun.com/exdb/mnist/`.

## Example

In this example we will compare logistic regression with linkgistic regression on an MNIST one vs the rest problem.

- You can check that everything is in place by testing the 0 vs the rest MNIST task on a small subset of the data: 

```
python -m linkgistic.em_linkgistic_demo --ntrain 30 --ntest 100
```
or 
```
python -m linkgistic.em_linkgistic_demo --ntrain 30 --ntest 100 --data_path "'$HOME/data/"
```

The last few lines of output should print the test error rates and give you a path where you will find more plots and outputs.
 
- Rerun it on the full dataset:

```
python -m linkgistic.em_linkgistic_demo
```

Note that this takes around 12 hours to complete due to the simple stochastic Expectation Maximisation scheme with sampled link functions, and the brutish default parameter choices.

- List the command line options (see the comments in the code for the meanings of the options):

```
python -m linkgistic.em_linkgistic_demo --help
```
